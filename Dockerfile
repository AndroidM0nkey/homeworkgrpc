FROM python:3-onbuild
EXPOSE 8000
COPY helloworld_pb2.py /
COPY helloworld_pb2_grpc.py /
COPY server.py /
CMD ["python", "server.py"]