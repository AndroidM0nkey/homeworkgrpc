# Copyright 2015 gRPC authors.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""The Python implementation of the GRPC helloworld.Greeter client."""

from __future__ import print_function

import logging

import grpc
import helloworld_pb2
import helloworld_pb2_grpc
import sys
import time

name = 'default'
ip = 'localhost:8000'
action = ''

def run():
    # NOTE(gRPC Python Team): .close() is possible on a channel and should be
    # used in circumstances in which the with statement does not fit the needs
    # of the code.
    with grpc.insecure_channel(ip) as channel:
        stub = helloworld_pb2_grpc.GreeterStub(channel)
        response = stub.SayHello(helloworld_pb2.HelloRequest(name=name))
    print("Server replied: " + response.message)

def disconnect():
    with grpc.insecure_channel(ip) as channel:
        stub = helloworld_pb2_grpc.GreeterStub(channel)
        response = stub.Disconnect(helloworld_pb2.DisconnectRequest(name=name))
    print("Server replied: " + response.message)

def get_table():
    with grpc.insecure_channel(ip) as channel:
        stub = helloworld_pb2_grpc.GreeterStub(channel)
        response = stub.GetTable(helloworld_pb2.TableRequest(req=True))
    print(response)


if __name__ == '__main__':
    print('Enter your name:')
    name = input()
    print('Enter ip:port of the server')
    ip = input()

    logging.basicConfig()
    run()

    while(True):
        print('Enter what you want to do (get_table/disconnect)')
        action = input()
        if action == 'disconnect':
            disconnect()
            sys.exit(0)
        elif action == 'get_table':
            get_table()
        else:
            print('incorrect input')
        time.sleep(1)
    
